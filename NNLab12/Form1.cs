﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace NNLab12
{
    public partial class Form1 : Form
    {
        private const int NeuronCount = 5,
            NeuronWeight = 8 * 8;
        private NeuralNetwork _neuralNetwork;

        private string _lettersFilename = "Letters.txt";
        private string _neuralNetworkFilename = "NeuralNetwork.txt";

        public Form1()
        {
            InitializeComponent();
            using (var reader = new StreamReader(_lettersFilename))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                    comboBox1.Items.Add(line);
                comboBox1.Items.Add("Nothing");
            }
            comboBox1.SelectedIndex = comboBox1.Items.Count - 1;
            labelResult.Text = "Nothing";

            try
            {
                _neuralNetwork = new NeuralNetwork(_neuralNetworkFilename);
            }
            catch (Exception e)
            {
                _neuralNetwork = new NeuralNetwork(NeuronCount, NeuronWeight);
                _neuralNetwork.Save(_neuralNetworkFilename);
            }

        }



        private void buttonReset_Click(object sender, EventArgs e)
        {
            drawPanel1.PaintedCells.Clear();
            drawPanel1.Invalidate();
        }

        private List<int> GetInputData()
        {
            int[] inputData = new int[8*8];
            
            foreach (var cell in drawPanel1.PaintedCells)
            {
                inputData[cell.X * 8 + cell.Y] = 1;
            }
            return inputData.ToList();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void buttonTrain_Click(object sender, EventArgs e)
        {
            var inputData = GetInputData();
            int index = comboBox1.SelectedIndex;
            _neuralNetwork.Update(inputData, index);
            _neuralNetwork.Save(_neuralNetworkFilename);
        }

        private void buttonShowResult_Click(object sender, EventArgs e)
        {
            var inputData = GetInputData();
            var answers = _neuralNetwork.GetNeuronAnswers(inputData);
            var results = answers
                .Select((a, i) => Tuple.Create(a, i))
                .Where(t => t.Item1 == 1)
                .Select(t => comboBox1.Items[t.Item2]);
            if (!results.Any())
                labelResult.Text = comboBox1.Items[comboBox1.Items.Count - 1].ToString();
            else
                labelResult.Text = string.Join(", ", results);
        }
    }
}
