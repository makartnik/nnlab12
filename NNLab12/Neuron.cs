﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace NNLab12
{
    public class Neuron
    {
        public IList<double> Weights { get; }

        public Neuron(int size)
        {
            Weights = new List<double>(new double[size]);
        }

        public int F(List<int> inputData)
        {
            if (inputData.Count!=Weights.Count)
                throw new ArgumentException(nameof(inputData));
            double sum = inputData
                .Select((item, index) => item * Weights[index])
                .Sum();
            return sum >= 0 ? 1 : 0;
        }

        public void UpdateWeights(IEnumerable<int> indices, double value)
        {
            foreach (int index in indices)
                Weights[index] += value;
        }
    }
}