﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace NNLab12
{
    public partial class DrawPanel : Control
    {

        public ISet<Point> PaintedCells { get; }

        public int RowCount { get; set; }
        public int ColumnCount { get; set; }

        public Color GridColor { get; set; }

        public Color CellColor { get; set; }

        private bool _isPaint;

        public DrawPanel()
        {
            InitializeComponent();
            PaintedCells = new HashSet<Point>();
            MouseDown += DrawPanel_MouseDown;
            MouseMove += DrawPanel_MouseMove;
            MouseUp += DrawPanel_MouseUp;
            MouseLeave += DrawPanel_MouseLeave;

        }

        private void DrawPanel_MouseLeave(object sender, EventArgs e)
        {
            _isPaint = false;
        }

        private void DrawPanel_MouseUp(object sender, MouseEventArgs e)
        {
            _isPaint = false;
        }

        private void DrawPanel_MouseMove(object sender, MouseEventArgs e)
        {
            if (_isPaint)
            {
                var cursorPosition = PointToClient(Cursor.Position);
                int x = cursorPosition.X * ColumnCount / Width,
                    y = cursorPosition.Y * RowCount / Height;
                if (x >= ColumnCount || y >= RowCount || x<0 || y<0)
                {
                    _isPaint = false;
                    return;
                }
                bool isAdded = PaintedCells.Add(new Point(x,y));
                if (isAdded)
                    Invalidate();
            }
        }

        private void DrawPanel_MouseDown(object sender, MouseEventArgs e)
        {
            _isPaint = true;
            var cursorPosition = PointToClient(Cursor.Position);
            bool isAdded = PaintedCells.Add(new Point(cursorPosition.X * ColumnCount / Width, cursorPosition.Y * RowCount / Height));
            if (isAdded)
                Invalidate();
        }

        protected override void OnPaint(PaintEventArgs pe)
        {
            base.OnPaint(pe);

            using (var g = CreateGraphics())
            {
                var brush = new SolidBrush(CellColor);
                foreach (var cell in PaintedCells)
                {
                    int x = cell.X * Width / ColumnCount;
                    int y = cell.Y * Height / RowCount;
                    int width = (cell.X + 1) * Width / ColumnCount - x;
                    int height = (cell.Y + 1) * Height / RowCount - y;
                    
                    g.FillRectangle(brush, x,y,width,height);
                }
                var pen = new Pen(GridColor);
                for (int r = 0; r < RowCount; r++)
                    g.DrawLine(pen, 0, Height * r / RowCount, Width - 1, Height * r / RowCount);
                for (int c = 0; c < ColumnCount; c++)
                    g.DrawLine(pen, Width * c / ColumnCount, 0, Width * c / ColumnCount, Height - 1);
                g.DrawLine(pen, 0, Height - 1, Width - 1, Height - 1);
                g.DrawLine(pen, Width - 1, 0, Width - 1, Height - 1);
            }
        }
    }
}
