﻿namespace NNLab12
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.drawPanel1 = new NNLab12.DrawPanel();
            this.buttonReset = new System.Windows.Forms.Button();
            this.buttonTrain = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.buttonShowResult = new System.Windows.Forms.Button();
            this.labelResult = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // drawPanel1
            // 
            this.drawPanel1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.drawPanel1.CellColor = System.Drawing.Color.Black;
            this.drawPanel1.ColumnCount = 8;
            this.drawPanel1.GridColor = System.Drawing.Color.Gray;
            this.drawPanel1.Location = new System.Drawing.Point(12, 12);
            this.drawPanel1.Name = "drawPanel1";
            this.drawPanel1.RowCount = 8;
            this.drawPanel1.Size = new System.Drawing.Size(300, 300);
            this.drawPanel1.TabIndex = 0;
            this.drawPanel1.Text = "drawPanel1";
            // 
            // buttonReset
            // 
            this.buttonReset.Location = new System.Drawing.Point(12, 318);
            this.buttonReset.Name = "buttonReset";
            this.buttonReset.Size = new System.Drawing.Size(75, 23);
            this.buttonReset.TabIndex = 1;
            this.buttonReset.Text = "Reset";
            this.buttonReset.UseVisualStyleBackColor = true;
            this.buttonReset.Click += new System.EventHandler(this.buttonReset_Click);
            // 
            // buttonTrain
            // 
            this.buttonTrain.Location = new System.Drawing.Point(545, 12);
            this.buttonTrain.Name = "buttonTrain";
            this.buttonTrain.Size = new System.Drawing.Size(75, 23);
            this.buttonTrain.TabIndex = 2;
            this.buttonTrain.Text = "Train";
            this.buttonTrain.UseVisualStyleBackColor = true;
            this.buttonTrain.Click += new System.EventHandler(this.buttonTrain_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(375, 12);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 24);
            this.comboBox1.TabIndex = 3;
            // 
            // buttonShowResult
            // 
            this.buttonShowResult.Location = new System.Drawing.Point(375, 55);
            this.buttonShowResult.Name = "buttonShowResult";
            this.buttonShowResult.Size = new System.Drawing.Size(93, 23);
            this.buttonShowResult.TabIndex = 4;
            this.buttonShowResult.Text = "Show result";
            this.buttonShowResult.UseVisualStyleBackColor = true;
            this.buttonShowResult.Click += new System.EventHandler(this.buttonShowResult_Click);
            // 
            // labelResult
            // 
            this.labelResult.AutoSize = true;
            this.labelResult.Location = new System.Drawing.Point(518, 58);
            this.labelResult.Name = "labelResult";
            this.labelResult.Size = new System.Drawing.Size(46, 17);
            this.labelResult.TabIndex = 5;
            this.labelResult.Text = "label1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(632, 493);
            this.Controls.Add(this.labelResult);
            this.Controls.Add(this.buttonShowResult);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.buttonTrain);
            this.Controls.Add(this.buttonReset);
            this.Controls.Add(this.drawPanel1);
            this.Name = "Form1";
            this.Text = "NNLab12";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DrawPanel drawPanel1;
        private System.Windows.Forms.Button buttonReset;
        private System.Windows.Forms.Button buttonTrain;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button buttonShowResult;
        private System.Windows.Forms.Label labelResult;
    }
}

