﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace NNLab12
{
    public class NeuralNetwork
    {
        private List<Neuron> _neurons;
        public double Nu { get; private set; }
        private const double DNu = 0.95;



        public NeuralNetwork(int neuronCount, int neuronSize)
        {
            Nu = 1;
            _neurons = new List<Neuron>();
            for (int i = 0; i < neuronCount; i++)
                _neurons.Add(new Neuron(neuronSize));
        }

        public NeuralNetwork(string filename)
        {
            using (var reader = new StreamReader(filename))
            {
                Nu = double.Parse(reader.ReadLine());
                int neuronCount = int.Parse(reader.ReadLine());
                _neurons = new List<Neuron>();
                for (int i = 0; i < neuronCount; i++)
                {
                    int weightCount = int.Parse(reader.ReadLine());
                    var neuron = new Neuron(weightCount);
                    for (int j = 0; j < weightCount; j++)
                    {
                        double weight = double.Parse(reader.ReadLine());
                        neuron.Weights[j] = weight;
                    }
                    _neurons.Add(neuron);
                }
            }
        }

        public IEnumerable<int> GetNeuronAnswers(List<int> inputData)
        {
            return _neurons
                .Select(n => n.F(inputData))
                .ToList();
        }

        public void Update(List<int> inputData, int correctIndex)
        {
            var answers = GetNeuronAnswers(inputData).ToList();
            var indices = inputData
                .Select((value, index) => Tuple.Create(value, index))
                .Where(t => t.Item1 == 1)
                .Select(t => t.Item2)
                .ToList();

            for (int i = 0; i < _neurons.Count; i++)
            {
                if (i == correctIndex && answers[i] != 1)
                    _neurons[i].UpdateWeights(indices, Nu);
                if (i != correctIndex && answers[i] == 1)
                    _neurons[i].UpdateWeights(indices, -Nu);
            }

            Nu *= DNu;
        }

        public void Save(string filename)
        {
            using (var writer = new StreamWriter(filename))
            {
                writer.WriteLine(Nu);
                writer.WriteLine(_neurons.Count);
                foreach (var neuron in _neurons)
                {
                    writer.WriteLine(neuron.Weights.Count);
                    foreach (var weight in neuron.Weights)
                    {
                        writer.WriteLine(weight);
                    }
                }
            }
        }
    }
}